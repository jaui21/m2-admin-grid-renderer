<?php

namespace [VENDOR]\[MODULE]\[PATH TO THE FILE];   //e.g. TemplateMonster\ShopByBrand\Block\Adminhtml\Brand\Tab\Renderer;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class GetBrand extends AbstractRenderer
{
    public function __construct(
       // \Magento\Framework\UrlInterface $urlBuilder,
       // \Magento\Store\Model\StoreManagerInterface $storeManager,
        $data = []
    ) {
       // $this->_storeManager = $storeManager;
    }
    public function render(DataObject $row)
    {
        $value = $row->getAttributeText('brand_id');        
        
        if($value == "-- Please Select --") {
            return ""; 
        }else {
           return $value;
        }
    }
}